////////////////////////////////////////////////////////////////////////
// DirDialog.h: interface for the CDirDialog class.
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DIRDIALOG_H__62FFAC92_1DEE_11D1_B87A_0060979CDF6D__INCLUDED_)
#define AFX_DIRDIALOG_H__62FFAC92_1DEE_11D1_B87A_0060979CDF6D__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

BOOL ZDirDialogSetRegistryKey(CString _FullKeyPathNoRoot, CString _Item, CString _Value);
BOOL ZDirDialogGetRegistryKey(CString _FullKeyPathNoRoot, CString _Item, CString& _Value);

class CDirDialog
{
public:

    CDirDialog(CString _RegItem = "", CString _SelDir = "",
		CString _Title = "��ѡ��һ��Ŀ¼", CString _WindowTitle = "���", 
		BOOL _HasStatus = FALSE, BOOL _HasEditBox = FALSE, BOOL _UseNewUI = FALSE);
    virtual ~CDirDialog();

    BOOL DoModal(CWnd* _pWndParent = NULL);

	void SetWindowTitle(CString _Title){m_WindowTitle = _Title;}
	void SetTitle(CString _Title){m_Title = _Title;}
	void SetSelDir(CString _Dir){m_SelDir = _Dir;}
	void SetRootDir(CString _Dir){m_InitDir = _Dir;}
	void SetUseRegistry(BOOL _Flag){m_IsUseRegistry = _Flag;}
	static void SetRegistryKey(CString strItem, CString strValue);
	static void GetRegistryKey(CString strItem, CString& strValue);

	CString GetWindowText(){return m_WindowTitle;}
	CString GetTitle(){return m_Title;}
	CString GetPathname(){return m_Path;}

protected:
	BOOL m_IsUseRegistry;
	CString m_RegItem;
    CString m_WindowTitle;
    CString m_Path;
    CString m_InitDir;
    CString m_SelDir;
    CString m_Title;
    int  m_ImageIdx;
	BOOL m_HasStatus;
	BOOL m_HasEditBox;
	BOOL m_UseNewUI;
	BROWSEINFO m_Info;
	BOOL m_IsEditBoxInputOK;

private:

    static int __stdcall CDirDialog::BrowseCtrlCallback(HWND hwnd, UINT uMsg, LPARAM lParam, LPARAM lpData);
};

#endif // !defined(AFX_DIRDIALOG_H__62FFAC92_1DEE_11D1_B87A_0060979CDF6D__INCLUDED_)

