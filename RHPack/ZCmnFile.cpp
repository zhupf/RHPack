#include "stdafx.h"
#include "ZCmnFile.h"

ZCmnFile::ZCmnFile(string _FileName/* = ""*/)
: m_FileName(_FileName)
, m_hFile(INVALID_HANDLE_VALUE)
, m_FileSize(0)
{
	if (!_FileName.empty()) {
		OpenFile(_FileName);
	}
}

ZCmnFile::~ZCmnFile(void)
{
	CloseFile();
}

bool ZCmnFile::CreateFile(string _FileName, bool _IsCreateAlways/* = true*/, DWORD dwShareMode/* = FILE_SHARE_READ*/)
{
	HANDLE hFile = ::CreateFile(_FileName.c_str(),GENERIC_READ|GENERIC_WRITE,dwShareMode,NULL,_IsCreateAlways?CREATE_ALWAYS:CREATE_NEW,FILE_ATTRIBUTE_NORMAL,NULL);
	CloseHandle(hFile);
	return (hFile != INVALID_HANDLE_VALUE);
}

bool ZCmnFile::TruncateFile(string _FileName)
{	
	HANDLE hFile = ::CreateFile(_FileName.c_str(),GENERIC_WRITE,FILE_SHARE_READ,NULL,TRUNCATE_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
	CloseHandle(hFile);
	return (hFile != INVALID_HANDLE_VALUE);
}

// 打开文件
bool ZCmnFile::OpenFile(string _FileName, bool _IsOpenAlways/* = false*/, bool _IsTruncate/* = false*/, 
							DWORD dwAccess/* = GENERIC_READ|GENERIC_WRITE*/, DWORD dwShareMode/* = FILE_SHARE_READ*/, DWORD dwFlagsAndAttributes/* = FILE_ATTRIBUTE_NORMAL*/)
{
	CloseFile();

	m_hFile = ::CreateFile(_FileName.c_str(),dwAccess,dwShareMode,NULL,_IsOpenAlways?OPEN_ALWAYS:OPEN_EXISTING,dwFlagsAndAttributes,NULL);
	if (INVALID_HANDLE_VALUE == m_hFile) {
		return false;
	}
	if (_IsTruncate) {
		ZFileSetSize(0);
	}

	m_FileName = _FileName;
	return true;
}

// 关闭文件
void ZCmnFile::CloseFile()
{
	if (INVALID_HANDLE_VALUE != m_hFile) {
		::CloseHandle(m_hFile);
		m_hFile = INVALID_HANDLE_VALUE;
	}
	m_FileName = "";
	m_FileSize = 0;
}

// 锁定文件区域
bool ZCmnFile::ZLockFile(__int64 _Offset,__int64 _LockLen)
{
	if (INVALID_HANDLE_VALUE != m_hFile) {
		LARGE_INTEGER _LiOffset;
		_LiOffset.QuadPart = _Offset;
		LARGE_INTEGER _LiLockLen;
		_LiLockLen.QuadPart = _LockLen;
		return LockFile(m_hFile,_LiOffset.LowPart,_LiOffset.HighPart,_LiLockLen.LowPart,_LiLockLen.HighPart);
	}
	else {
		return false;
	}
}

// 解锁文件区域
bool ZCmnFile::ZUnlockFile(__int64 _Offset,__int64 _UnLockLen)
{
	if (INVALID_HANDLE_VALUE != m_hFile) {
		LARGE_INTEGER _LiOffset;
		_LiOffset.QuadPart = _Offset;
		LARGE_INTEGER _LiUnLockLen;
		_LiUnLockLen.QuadPart = _UnLockLen;
		return UnlockFile(m_hFile,_LiOffset.LowPart,_LiOffset.HighPart,_LiUnLockLen.LowPart,_LiUnLockLen.HighPart);
	}
	else {
		return false;
	}
}

// 移动文件指针
__int64 ZCmnFile::ZFileSeek(__int64 _Distance, DWORD _MoveMethod/* = FILE_CURRENT*/)
{
	if (INVALID_HANDLE_VALUE != m_hFile) {
		LARGE_INTEGER _Li;
		_Li.QuadPart = _Distance;
		LARGE_INTEGER _LiNew;
		if (::SetFilePointerEx(m_hFile, _Li, &_LiNew, _MoveMethod)) {
			return _LiNew.QuadPart;
		}
	}
	return -1;
}

// 获取当前文件指针
__int64 ZCmnFile::ZFileTell()
{
	if (INVALID_HANDLE_VALUE == m_hFile) {
		return INVALID_SET_FILE_POINTER;
	}
	return ZFileSeek(0,FILE_CURRENT);
}

// 获取文件大小
__int64 ZCmnFile::ZFileGetSize()
{
	LARGE_INTEGER _Li;
	if (INVALID_HANDLE_VALUE != m_hFile && ::GetFileSizeEx(m_hFile,&_Li)) {
		return _Li.QuadPart;
	}
	return -1;
}

// 设置文件大小(扩大,缩小长度)
bool ZCmnFile::ZFileSetSize(__int64 _NewLen)
{
	if (INVALID_HANDLE_VALUE == m_hFile) {
		return false;
	}
	__int64 _Offset = ZFileTell();
	if (-1!=ZFileSeek(_NewLen,FILE_BEGIN)) {
		bool _Ret = ::SetEndOfFile(m_hFile);
		if (_Ret && _Offset < _NewLen) {
			_Ret = (_Offset == ZFileSeek(_Offset,FILE_BEGIN));
		}
		return _Ret;
	}
	return false;
}

// 是否已经到文件结尾
bool ZCmnFile::ZIsFileEOF()
{
	if (INVALID_HANDLE_VALUE == m_hFile) {
		return false;
	}
	__int64 _FileLen = ZFileGetSize();
	return (-1 != _FileLen) && (ZFileTell() == _FileLen);
}

// 是否文件已经打开
bool ZCmnFile::ZIsFileOpen()
{
	if (INVALID_HANDLE_VALUE == m_hFile) {
		return false;
	}
	BY_HANDLE_FILE_INFORMATION _Fi;
	return NULL != ::GetFileInformationByHandle(m_hFile,&_Fi);
}

// 读取文件
bool ZCmnFile::ReadFile(LPVOID _pBuffer,DWORD _BytesToRead,LPDWORD _pBytesReaded/* = NULL*/)
{
	if (INVALID_HANDLE_VALUE == m_hFile) {
		return false;
	}
	DWORD _DummyLen;
	LPDWORD lpBytesReaded = _pBytesReaded?_pBytesReaded:&_DummyLen;
	return ::ReadFile(m_hFile,_pBuffer,_BytesToRead,lpBytesReaded,NULL) && (*lpBytesReaded>0);
}

// 写入文件
bool ZCmnFile::WriteFile(LPVOID _pBuffer,DWORD _BytesToWrite,LPDWORD _pBytesWritten/* = NULL*/)
{
	if (INVALID_HANDLE_VALUE == m_hFile) {
		return false;
	}
	DWORD _DummyLen;
	LPDWORD lpBytesWritten = _pBytesWritten?_pBytesWritten:&_DummyLen;
	return ::WriteFile(m_hFile,_pBuffer,_BytesToWrite,lpBytesWritten,NULL) && (*lpBytesWritten>0);
}

void ZCmnFile::Flush()
{
	if (INVALID_HANDLE_VALUE == m_hFile) {
		return;
	}
	::FlushFileBuffers(m_hFile);
}

bool ZCmnFile::Text(const string& _Text,BOOL _AutoNewLine/* = TRUE*/,LPDWORD _pBytesWritten/* = NULL*/)
{
	if (INVALID_HANDLE_VALUE == m_hFile) {
		return false;
	}
	DWORD _DummyLen;
	LPDWORD lpBytesWritten = _pBytesWritten?_pBytesWritten:&_DummyLen;
	BOOL _Ret = ::WriteFile(m_hFile,(LPVOID)_Text.c_str(),_Text.size(),lpBytesWritten,NULL) && (*lpBytesWritten>0);
	if (_Ret && _AutoNewLine) {
		::WriteFile(m_hFile,"\r\n",2,&_DummyLen,NULL);
	}
	return _Ret;
}