
// RHPackDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "RHPack.h"
#include "RHPackDlg.h"
#include "afxdialogex.h"
#include "DirDialog.h"
#include "Arc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CRHPackDlg 对话框

CRHPackDlg::CRHPackDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CRHPackDlg::IDD, pParent)
	, m_IdxFile(_T(""))
	, m_ExtDir(_T(""))
	, m_ResDir(_T(""))
	, m_UseNewArchPckIdxHash(g_UseNewArchPckIdxHash)
	, m_NoPckIdx(g_NoPckIdx)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CRHPackDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_PCKFILE, m_IdxFile);
	DDX_Text(pDX, IDC_EDIT_EXTDIR, m_ExtDir);
	DDX_Text(pDX, IDC_EDIT_PACKDIR, m_ResDir);
	DDX_Check(pDX, IDC_CHECK_USENEW, m_UseNewArchPckIdxHash);
	DDX_Check(pDX, IDC_CHECK_USENOIDX, m_NoPckIdx);
}

BEGIN_MESSAGE_MAP(CRHPackDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_ARCDIR, &CRHPackDlg::OnBnClickedButtonArcdir)
	ON_BN_CLICKED(IDC_BUTTON_SELPCK, &CRHPackDlg::OnBnClickedButtonSelpck)
	ON_BN_CLICKED(IDC_BUTTON_EXT, &CRHPackDlg::OnBnClickedButtonExt)
	ON_BN_CLICKED(IDC_BUTTON_SELUNPACKDIR, &CRHPackDlg::OnBnClickedButtonSelunpackdir)
	ON_BN_CLICKED(IDC_BUTTON_SELPACDIR, &CRHPackDlg::OnBnClickedButtonSelpacdir)
	ON_BN_CLICKED(IDC_CHECK_USENEW, &CRHPackDlg::OnBnClickedCheckUsenew)
	ON_BN_CLICKED(IDC_CHECK_USENOIDX, &CRHPackDlg::OnBnClickedCheckUsenoidx)
	ON_BN_CLICKED(IDC_BUTTON_NOPCKIDX, &CRHPackDlg::OnBnClickedButtonNopckidx)
END_MESSAGE_MAP()


// CRHPackDlg 消息处理程序

BOOL CRHPackDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CRHPackDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CRHPackDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CRHPackDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CRHPackDlg::OnBnClickedButtonSelpck()
{
	CFileDialog dlg(TRUE, "*.dat", "f00X.dat", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,"资源索引 (f00X.dat)|*.dat|所有文件 (*.*)|*.*||");
	if (dlg.DoModal() == IDOK) {
		m_IdxFile = dlg.GetPathName();
		UpdateData(FALSE);
	}
	SetDlgItemText(IDC_STATIC_INFO,"选择压缩包,再选择打包目录点打包,或者选解包目录点解包.");
}

void CRHPackDlg::OnBnClickedButtonSelpacdir()
{
	CDirDialog _Dlg("RHPack_ChooseResDir",m_ResDir,"选择要打包的目录","浏览",false,false,true);
	if (IDOK == _Dlg.DoModal()) {
		m_ResDir = _Dlg.GetPathname();
		UpdateData(FALSE);
	}
	SetDlgItemText(IDC_STATIC_INFO,"选择压缩包,再选择打包目录点打包,或者选解包目录点解包.");
}

void CRHPackDlg::OnBnClickedButtonSelunpackdir()
{
	CDirDialog _Dlg("RHPack_ChooseExtDir",m_ExtDir,"选择解压到目录","浏览",false,false,true);
	if (IDOK == _Dlg.DoModal()) {
		m_ExtDir = _Dlg.GetPathname();
		UpdateData(FALSE);
	}
	SetDlgItemText(IDC_STATIC_INFO,"选择压缩包,再选择打包目录点打包,或者选解包目录点解包.");
}

void CRHPackDlg::OnBnClickedButtonArcdir()
{
	UpdateData(TRUE);
	m_PckDir = m_IdxFile.Left(m_IdxFile.ReverseFind('\\')+1);
	if (!PathFileExists(m_ResDir)) {
		MessageBox("需要压缩的资源目录不存在!","错误",MB_OK);
		return;
	}
	SetDlgItemText(IDC_STATIC_INFO,_T("开始打包."));
	if (ArchiveDir((LPCTSTR)m_IdxFile,(LPCTSTR)m_PckDir,(LPCTSTR)m_ResDir)) {
		SetDlgItemText(IDC_STATIC_INFO,"打包完成!");
	}
	else{
		SetDlgItemText(IDC_STATIC_INFO,"打包错误!");
	}
}

void CRHPackDlg::OnBnClickedButtonExt()
{
	UpdateData(TRUE);
	if (!PathFileExists(m_IdxFile)) {
		MessageBox("资源索引文件f00X.dat不存在!","错误",MB_OK);
		return;
	}
	m_PckDir = m_IdxFile.Left(m_IdxFile.ReverseFind('\\')+1);
	if (m_ExtDir.IsEmpty()) {
		MessageBox("输出目录不能为空!","错误",MB_OK);
		return;
	}
	MkDir((LPCTSTR)m_ExtDir);
	SetDlgItemText(IDC_STATIC_INFO,_T("开始解包."));
	if (ExtractorDir((LPCTSTR)m_IdxFile,(LPCTSTR)m_PckDir,(LPCTSTR)m_ExtDir)) {
		SetDlgItemText(IDC_STATIC_INFO,"解包完成!");
	}
	else{
		SetDlgItemText(IDC_STATIC_INFO,"解包错误!");
	}
}

void CRHPackDlg::OnBnClickedCheckUsenew()
{
	UpdateData(TRUE);
	g_UseNewArchPckIdxHash = m_UseNewArchPckIdxHash;
}

void CRHPackDlg::OnBnClickedCheckUsenoidx()
{
	UpdateData(TRUE);
	g_NoPckIdx = m_NoPckIdx;
}

void CRHPackDlg::OnBnClickedButtonNopckidx()
{
	UpdateData(TRUE);
	if (!PathFileExists(m_IdxFile)) {
		MessageBox("资源索引文件f00X.dat不存在!","错误",MB_OK);
		return;
	}
	SetDlgItemText(IDC_STATIC_INFO,_T("开始混淆."));
	string _IdxFileBuff = UnZipIdexFileBuff((LPCTSTR)m_IdxFile);
	MakeIdxMapByIdxBuff(_IdxFileBuff,g_IdxMap);
	BOOL _NoPckIdx = g_NoPckIdx;
	g_NoPckIdx = TRUE;
	MakeIdxBuffByIdxMap(_IdxFileBuff, g_IdxMap);
	ZipIdexFileBuff((LPCTSTR)m_IdxFile,_IdxFileBuff);
	g_NoPckIdx = _NoPckIdx;
	SetDlgItemText(IDC_STATIC_INFO,_T("混淆完成."));
}
