#pragma once

#include "wtypes.h"
#include "winbase.h"
#include <shlwapi.h>
#pragma comment(lib,"shlwapi.lib")

#include <objbase.h>
#include <sstream>
#include <string>
#include <map>
#include <vector>
#include <algorithm>
#include <functional>

using std::stringstream;
using std::string;
using std::map;
using std::vector;

#define A2W_CP_S(lpa, cp) (\
	((_lpa = lpa) == NULL) ? NULL : (\
	(_convert = (::MultiByteToWideChar(cp, 0, lpa, lstrlenA(lpa)+1, NULL, 0) + 1)),\
	ATLA2WHELPER((LPWSTR) alloca(_convert), _lpa, _convert, (cp))))

#define W2A_CP_S(lpw, cp) (\
	((_lpw = lpw) == NULL) ? NULL : (\
	(_convert = (::WideCharToMultiByte(cp, 0, lpw, lstrlenW(_lpw)+1, NULL, 0, NULL, NULL) + 1), \
	ATLW2AHELPER((LPSTR) alloca(_convert), _lpw, _convert*sizeof(WCHAR), (cp)))))

#ifndef SAFE_CLOSEHANDLE
#	define SAFE_CLOSEHANDLE(h) { if (((HANDLE)(h)) != NULL) { ::CloseHandle((HANDLE)(h)); (h) = NULL; } }
#endif

#ifndef SAFE_RELEASE
#	define SAFE_RELEASE(p) { if ((p) != NULL) { p->Release(); (p) = NULL; } }
#endif

#ifndef SAFE_DELETE
#	define SAFE_DELETE(p) { if ((p) != NULL) { delete (p); (p) = NULL; } }
#endif

#ifndef SAFE_DELETE_ARRAY
#	define SAFE_DELETE_ARRAY(p)	{ if ((p) != NULL) { delete [](p); (p) = NULL; } }
#endif

#ifndef SAFE_CALLFUNPTR
#	define SAFE_CALLFUNPTR(fnPointer, functionName) if (fnPointer) functionName
#endif

#ifndef SAFE_CALLFUNPTR_CLEAN
#	define SAFE_CALLFUNPTR_CLEAN(fnPointer, functionName, initvalue) if (fnPointer) functionName;fnPointer = initvalue
#endif

class COleAutoInit
{
public:
	COleAutoInit() { CoInitialize(NULL); }
	~COleAutoInit() { CoUninitialize(); }
};

#define STRING_4_SPACE	"    "			// 4个空格
//#define STRING_NEW_LINE	"\r\n"			// 在Windows下的换行符
#define STRING_NEW_LINE	"\n"			// c++内的换行


//////////////////////////////////////////////////////////////////////////
// 函数说明:		帮助函数, 按指定的偏移与换行次数在strInOut中追加字符串strText
// nIndent:			缩进空格数(一次4空格)
// nNumNewLine:		需要换行的次数
// strText:			待追加字符串
// strInOut:		目标字符串(in, out)
static void HelpAppendString(int nIndent, int nNumNewLine, string strText, string &strInOut)
{
	string strIndent("");
	string strNewLine("");

	if (nIndent >= 0 && nIndent <= 10)
	{
		for (int i = 0; i < nIndent; ++i)
			strIndent += STRING_4_SPACE;
	}

	strInOut += strIndent;
	strInOut += strText;

	if (nNumNewLine == 1 || nNumNewLine == 2)
	{
		for (int i = 0; i < nNumNewLine; ++i)
			strNewLine += STRING_NEW_LINE;
	}

	strInOut += strNewLine;
}

inline bool isChar(char c1, char c2)
{
	return c1 == c2;
}

inline string& remove_char(string& str,char c)
{
	str.erase(std::remove_if(str.begin(), str.end(), std::bind2nd(std::ptr_fun(isChar),c)), str.end()); // 去掉字符
	return str;
}

inline string& remove_space(string& t)
{
	return remove_char(t,' ');
}

inline string& trans_tolower(string& t)
{
	std::transform(t.begin(),t.end(),t.begin(),::tolower); // 转小写
	return t;
}

inline string& trans_toupper(string& t)
{
	std::transform(t.begin(),t.end(),t.begin(),::toupper); // 转大写
	return t;
}

template <typename T> struct IsAllToStrSupport { enum { Support = true }; };
template <> struct IsAllToStrSupport<UINT8> { enum { Support = false }; };
template <> struct IsAllToStrSupport<INT8> { enum { Support = false }; };
template <> struct IsAllToStrSupport<UINT16> { enum { Support = false }; };
template <> struct IsAllToStrSupport<INT16> { enum { Support = false }; };

template <class T>
inline string AllToStr(T t,bool bHex = false,bool bUpper = false)
{
	string strTemp;
	stringstream convt;
	if (bHex) {
		convt << std::hex;
	}
	if (bUpper) {
		convt << std::uppercase;
	}
	if (IsAllToStrSupport<T>::Support) {
		convt << t; 
	}
	else {
		convt << (int)t; 
	}
	convt >> strTemp; 
	convt.clear();
	return strTemp;
}

template <class T>
inline T StrToAll(string t)
{
	T r;
	stringstream convt;
	convt << t;
	convt >> r; 
	convt.clear();
	return r;
}

// 重载
inline string AllToStr(string t)
{
	return t;
}

inline string AllToStr(bool t)
{
	if (t == true)
		return "true";
	else
		return "false";
}

template <>
inline int StrToAll(string t)
{
	int r;
	stringstream convt;
	if (find_if(t.begin(),t.end(),std::bind2nd(std::ptr_fun(isChar),'x')) != t.end() ||
		find_if(t.begin(),t.end(),std::bind2nd(std::ptr_fun(isChar),'X')) != t.end())
		convt << std::hex;

	convt << t;
	convt >> r; 
	convt.clear();
	return r;
}

template <>
inline string StrToAll(string t)
{
	return t;
}

template <>
inline bool StrToAll(string t)
{
	t.erase(std::remove_if(t.begin(), t.end(), std::bind2nd(std::ptr_fun(isChar),' ')), t.end()); // 去空格
	std::transform(t.begin(),t.end(),t.begin(),::tolower); // 转小写
	if (t == "true")
		return true;
	else
		return false;
}

inline string GetCurrentDir()
{
	CHAR _CurrentDir[MAX_PATH] = {0};
	::GetCurrentDirectoryA(MAX_PATH,_CurrentDir);
	return string(_CurrentDir);
}

inline string GetCurWorkPath()
{
	char szFullPath[MAX_PATH] = {0};
	GetModuleFileNameA(NULL, szFullPath, MAX_PATH);
	::PathRemoveFileSpecA(szFullPath);
	return szFullPath;
}

inline string ExtractFileName(string strPathName)
{
	char drive[_MAX_DRIVE];
	char dir[_MAX_DIR];
	char fname[_MAX_FNAME];
	char ext[_MAX_EXT];
	_splitpath_s(strPathName.c_str(), drive, _MAX_DRIVE, dir, _MAX_DIR, fname, _MAX_FNAME, ext, _MAX_EXT);

	return string(fname)+string(ext);
}

// 调试用,打印时间
inline void TimeBegin(DWORD& dwTimeStart)
{
	dwTimeStart = ::GetTickCount();
}

inline void TimePrint(int nIdx,DWORD& dwTimeStart,DWORD& dwTimeEnd)
{
	dwTimeEnd = ::GetTickCount();
	string strErr = AllToStr(nIdx) + ": " + AllToStr(dwTimeEnd-dwTimeStart) + "\n";
	dwTimeStart = dwTimeEnd;
	::OutputDebugStringA(strErr.c_str());
}