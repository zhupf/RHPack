
// RHPackDlg.h : 头文件
//

#pragma once


// CRHPackDlg 对话框
class CRHPackDlg : public CDialogEx
{
// 构造
public:
	CRHPackDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_RUSTYHEARTSARCHIVER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

public:
	CString m_IdxFile;
	CString m_PckDir;
	CString m_ExtDir;
	CString m_ResDir;
	BOOL m_UseNewArchPckIdxHash;

// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonArcdir();
	afx_msg void OnBnClickedButtonSelpck();
	afx_msg void OnBnClickedButtonExt();
	afx_msg void OnBnClickedButtonSelunpackdir();
	afx_msg void OnBnClickedButtonSelpacdir();
	afx_msg void OnBnClickedCheckUsenew();
	BOOL m_NoPckIdx;
	afx_msg void OnBnClickedCheckUsenoidx();
	afx_msg void OnBnClickedButtonNopckidx();
};
