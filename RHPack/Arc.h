#include <windows.h>
#include "zlib/zlib.h"
#include <stdio.h>
#include <string>
#include <vector>
#include <map>
using std::string;
using std::vector;
using std::map;

struct FileElem
{
	FileElem() 
		: NameLen(0)
		, ArchIdx(0)
		, FileLen(0)
		, CRC(0)
		, Offset(0)
	{
	}
	UINT16 NameLen;
	string Name;
	BYTE ArchIdx;
	DWORD FileLen;
	DWORD CRC;
	__int64 Offset;
};

extern BOOL g_UseNewArchPckIdxHash;
extern BOOL g_NoPckIdx;
extern map<string,FileElem> g_IdxMap;
int Decompress(unsigned char * compressed, unsigned int compressed_size, 
			   unsigned char * decompressed, unsigned int * decompressed_size);
int Compress(unsigned char * decompressed, unsigned int decompressed_size, 
			 unsigned char * compressed, unsigned int * compressed_size);

unsigned __stdcall PckArchThread(void* _pParam);
unsigned __stdcall UnPckArchThread(void* _pParam);
void MakeIdxMapByIdxBuff(const string& _IdxFileBuff,map<string,FileElem>& _IdxMap);
void MakeIdxBuffByIdxMap(string& _IdxFileBuff,map<string,FileElem>& _IdxMap);
void MkDir(string _FileOrDir,bool _IsFile = false);
void FindAllFile(const string& _RootName,const string& _DirName,vector<string>& _FileList);
string UnZipIdexFileBuff(const string& _IdxFileName);
BOOL ZipIdexFileBuff(const string& _IdxFileName, const string& _IdxFileBuff);
BOOL ExtractorDir(const string& _IdxFileName, const string& _PckFileDir, const string& _ResBaseDir);
BOOL Extractor(FileElem& _Fe,const string& _PckFile);
BOOL ArchiveDir(const string& _IdxFileName,const string& _PckFileDir, const string& _ResBaseDir);
BOOL Archive(FileElem& _Fe,const string& _PckFile);
