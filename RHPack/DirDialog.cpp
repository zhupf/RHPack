///////////////////////////////////////////////////////////////////////////
// DirDialog.cpp: implementation of the CDirDialog class.
//////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "DirDialog.h"
#include "shlobj.h"
#include "shlwapi.h"
#pragma comment(lib,"shlwapi.lib")

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#define IDC_STATUS_DIRBROWSE 0x3743   // 这个是状态信息的STATIC控件的ID,可能在其他版本下会变.请用Spy++查看
#define DISMISS_DIRDIALOG 1

//HKEY_CURRENT_USER\Software\ZDirDialog\_Item = "value"
CONST CHAR* ZDIRDIALOG_KEYPATH = "Software\\ZDirDialog";

BOOL ZDirDialogSetRegistryKey(CString _FullKeyPathNoRoot, CString _Item, CString _Value)
{
	HKEY hKey;
	long lResult;

	lResult = RegCreateKeyEx(HKEY_CURRENT_USER, (LPCTSTR)_FullKeyPathNoRoot, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL);
	if(lResult != ERROR_SUCCESS) return FALSE;

	lResult = RegSetValueEx(hKey, (LPCTSTR)_Item, 0, REG_SZ, (const BYTE *)(LPCTSTR)_Value, _Value.GetLength()+1);
	if(lResult != ERROR_SUCCESS) return FALSE;

	lResult = RegCloseKey(hKey);
	if(lResult != ERROR_SUCCESS) return FALSE;

	return TRUE;
}

BOOL ZDirDialogGetRegistryKey(CString _FullKeyPathNoRoot, CString _Item, CString& _Value)
{
	HKEY hKey;
	long lResult;
	DWORD dwValueLen = MAX_PATH;

	lResult = RegCreateKeyEx(HKEY_CURRENT_USER, (LPCTSTR)_FullKeyPathNoRoot, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL);
	if(lResult != ERROR_SUCCESS) return FALSE;

	lResult = RegQueryValueEx(hKey, (LPCTSTR)_Item, 0, NULL, (LPBYTE)_Value.GetBuffer(MAX_PATH),&dwValueLen);
	_Value.ReleaseBuffer();
	if(lResult != ERROR_SUCCESS) return FALSE;

	lResult = RegCloseKey(hKey);
	if(lResult != ERROR_SUCCESS) return FALSE;

	return TRUE;
}

// Callback function called by SHBrowseForFolder's browse control
// after initialization and when selection changes
int __stdcall CDirDialog::BrowseCtrlCallback(HWND hwnd, UINT uMsg, LPARAM lParam, LPARAM lpData)
{
    CDirDialog* pDirDialogObj = (CDirDialog*)lpData;
    if (uMsg == BFFM_INITIALIZED)
    {
        if(pDirDialogObj->m_SelDir.IsEmpty())
		{
			::GetCurrentDirectory(MAX_PATH,pDirDialogObj->m_SelDir.GetBuffer(MAX_PATH));
			pDirDialogObj->m_SelDir.ReleaseBuffer();
		}
		::SendMessage(hwnd, BFFM_SETSELECTION, TRUE, (LPARAM)(LPCTSTR)(pDirDialogObj->m_SelDir));
        if(!pDirDialogObj->m_WindowTitle.IsEmpty())
            ::SetWindowText(hwnd, (LPCTSTR) pDirDialogObj->m_WindowTitle);
    }
    else if(uMsg == BFFM_SELCHANGED)
    {
        LPITEMIDLIST pidl = (LPITEMIDLIST) lParam;
		TCHAR _Selection[MAX_PATH] = {0};
		pDirDialogObj->m_Path.Empty();
        if(::SHGetPathFromIDList(pidl, _Selection))
		{
			pDirDialogObj->m_Path = _Selection;
		}
		if(pDirDialogObj->m_HasStatus)
		{
			if (GetDlgItem(hwnd,IDC_STATUS_DIRBROWSE))
				::PathSetDlgItemPath(hwnd,IDC_STATUS_DIRBROWSE,_Selection);
			else
				::SendMessage(hwnd, BFFM_SETSTATUSTEXT , 0, (LPARAM)_Selection);
		}
		::SendMessage(hwnd, BFFM_ENABLEOK, 0, !pDirDialogObj->m_Path.IsEmpty());
    }
	else if (uMsg == BFFM_VALIDATEFAILED)
	{
		CString _Selection = (TCHAR*)lParam;
		if (-1 == ::PathGetDriveNumber(_Selection))
		{
			CString _SelDir = pDirDialogObj->m_Path;
			_SelDir.TrimRight("\\/");
			if (!_SelDir.IsEmpty()) {
				::PathRemoveFileSpecA(_SelDir.GetBuffer());
				_SelDir.ReleaseBuffer();
				_Selection = _SelDir + "\\" + _Selection;
			}
		}

		if (!::PathFileExists(_Selection)) {
			return DISMISS_DIRDIALOG;
		}
		else {
			SendMessage(hwnd,BFFM_SETSELECTION,(WPARAM)TRUE,(LPARAM)(LPCTSTR)_Selection);
			pDirDialogObj->m_IsEditBoxInputOK = TRUE;
		}
	}
  return 0;
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDirDialog::CDirDialog(CString _RegItem/* = ""*/,
					   CString _SelDir/* = ""*/,
					   CString _Title/* = "请选择一个目录"*/,
					   CString _WindowTitle/* = "浏览"*/,
					   BOOL _HasStatus/* = FALSE*/,
					   BOOL _HasEditBox/* = FALSE*/,
					   BOOL _UseNewUI/* = FALSE*/)
: m_RegItem(_RegItem)
, m_SelDir(_SelDir)
, m_Title(_Title)
, m_WindowTitle(_WindowTitle)
, m_HasStatus(_HasStatus)
, m_HasEditBox(_HasEditBox)
, m_UseNewUI(_UseNewUI)
{
	m_IsUseRegistry = TRUE;
    m_Path.Empty();
    m_InitDir.Empty();
    m_ImageIdx = 0;
	m_IsEditBoxInputOK = FALSE;
}

CDirDialog::~CDirDialog()
{
}

BOOL CDirDialog::DoModal(CWnd* _pWndParent/* = NULL*/)
{
    if(!m_SelDir.IsEmpty())
    {
		m_SelDir.Replace('/','\\');
		::PathRemoveBackslash(m_SelDir.Trim().GetBuffer(MAX_PATH));
		m_SelDir.ReleaseBuffer();
    }
	else if (!m_RegItem.IsEmpty() && m_IsUseRegistry)
	{
		ZDirDialogGetRegistryKey(ZDIRDIALOG_KEYPATH,m_RegItem,m_SelDir);
	}

    LPITEMIDLIST pidl;
    ZeroMemory((PVOID)&m_Info,sizeof(BROWSEINFO));

    if (!m_InitDir.IsEmpty())
    {
        OLECHAR       olePath[MAX_PATH];
        ULONG         chEaten;
        ULONG         dwAttributes;
        HRESULT       hr;
        LPSHELLFOLDER pDesktopFolder;
        //
        // Get a pointer to the Desktop's IShellFolder interface.
        //
        if (SUCCEEDED(SHGetDesktopFolder(&pDesktopFolder)))
        {
            //
            // IShellFolder::ParseDisplayName requires the file name be in Unicode.
            //
            MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, m_InitDir, -1,
                                olePath, MAX_PATH);

            //
            // Convert the path to an ITEMIDLIST.
            //
            hr = pDesktopFolder->ParseDisplayName(NULL,
                                                NULL,
                                                olePath,
                                                &chEaten,
                                                &pidl,
                                                &dwAttributes);
            if (SUCCEEDED(hr))
				m_Info.pidlRoot = pidl;
        }
    }
    m_Info.hwndOwner = _pWndParent == NULL ? NULL : _pWndParent->GetSafeHwnd();
    m_Info.pszDisplayName = m_InitDir.GetBuffer(MAX_PATH);
    m_Info.lpszTitle = m_Title;
	if (m_UseNewUI) {
		m_Info.ulFlags = BIF_RETURNFSANCESTORS
			| BIF_RETURNONLYFSDIRS | BIF_USENEWUI | BIF_VALIDATE;
		m_HasStatus = false;
		m_HasEditBox = true;
	}
	else {
		m_Info.ulFlags = BIF_RETURNFSANCESTORS
			| BIF_RETURNONLYFSDIRS// | BIF_USENEWUI
			| (m_HasStatus ? BIF_STATUSTEXT : 0)
			| (m_HasEditBox ? BIF_EDITBOX|BIF_VALIDATE : 0);
	}

    m_Info.lpfn = BrowseCtrlCallback;  // address of callback function
    m_Info.lParam = (LPARAM)this;      // pass address of object to callback function

	pidl = ::SHBrowseForFolder(&m_Info);
	m_ImageIdx = m_Info.iImage;
	m_InitDir.ReleaseBuffer();

	if (pidl) {
		::SHGetPathFromIDList(pidl, m_Path.GetBuffer(MAX_PATH));
		m_Path.ReleaseBuffer();
	}
	if (m_IsEditBoxInputOK){
		pidl = (LPITEMIDLIST)1;
		m_IsEditBoxInputOK = FALSE;
	}

	if (!m_RegItem.IsEmpty() && m_IsUseRegistry) {
		ZDirDialogSetRegistryKey(ZDIRDIALOG_KEYPATH,m_RegItem,m_Path);
	}
    return (pidl != NULL);
}

void CDirDialog::SetRegistryKey(CString strItem, CString strValue)
{
	ZDirDialogSetRegistryKey(ZDIRDIALOG_KEYPATH,strItem,strValue);
}

void CDirDialog::GetRegistryKey(CString strItem, CString& strValue)
{
	ZDirDialogGetRegistryKey(ZDIRDIALOG_KEYPATH,strItem,strValue);
}