#pragma once

#include <string>
using std::string;

// 通用的文件类,支持大文件(64位文件偏移)
class ZCmnFile
{
public:
	ZCmnFile(string _FileName = "");
	~ZCmnFile();

public:
	// 创建文件
	static bool CreateFile(string _FileName, bool _IsCreateAlways = true, DWORD dwShareMode = FILE_SHARE_READ);

	// 清空文件
	static bool TruncateFile(string _FileName);

public:
	bool IsOpened() { return (INVALID_HANDLE_VALUE != m_hFile); }

	// 打开文件
	bool OpenFile(string _FileName, bool _IsOpenAlways = false, bool _IsTruncate = false, 
		DWORD dwAccess = GENERIC_READ|GENERIC_WRITE, DWORD dwShareMode = FILE_SHARE_READ, DWORD dwFlagsAndAttributes = FILE_ATTRIBUTE_NORMAL);

	// 关闭文件
	void CloseFile();

	// 获取文件大小
	__int64 ZFileGetSize();

	// 设置文件大小(扩大,缩小长度)
	bool ZFileSetSize(__int64 _NewLen);

	// 锁定文件区域
	bool ZLockFile(__int64 _Offset,__int64 _LockLen);

	// 解锁文件区域
	bool ZUnlockFile(__int64 _Offset,__int64 _UnLockLen);

	// 移动文件指针
	__int64 ZFileSeek(__int64 _Distance, DWORD _MoveMethod = FILE_CURRENT);

	// 获取当前文件指针
	__int64 ZFileTell();

	// 是否已经到文件结尾
	bool ZIsFileEOF();

	// 是否文件已经打开
	bool ZIsFileOpen();

	// 读取文件
	bool ReadFile(LPVOID _pBuffer,DWORD _BytesToRead,LPDWORD _pBytesReaded = NULL);

	// 写入文件
	bool WriteFile(LPVOID _pBuffer,DWORD _BytesToWrite,LPDWORD _pBytesWritten = NULL);

	void Flush();

	bool Text(const string& _Text,BOOL _AutoNewLine = TRUE,LPDWORD _pBytesWritten = NULL);

	string GetFileName() { return m_FileName; }

private:
	string m_FileName;
	HANDLE m_hFile;
	__int64 m_FileSize;
};
